package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long> {
    private Long from;
    private List<Long> to;
    private String message;
    private LocalDateTime timestamp;
    private Long parent;

    public Message(Long from, List<Long> to, String message, LocalDateTime timestamp) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.timestamp = timestamp;
        this.parent = null;
    }

    public Message(Long from, List<Long> to, String message, LocalDateTime timestamp, Long parent) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.timestamp = timestamp;
        this.parent = parent;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public List<Long> getTo() {
        return to;
    }

    public void setTo(List<Long> to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, message, timestamp);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof Message) {
            Message that = (Message) obj;
            return getTo().equals(that.getTo()) &&
                    getFrom().equals(that.getFrom()) &&
                    getTimestamp().equals(that.getTimestamp());
        }
        return false;
    }

    @Override
    public String toString() {
        return "Message {\n" +
                "\tfrom: " + from + "\n" +
                "\tto: " + to + "\n" +
                "\tmessage: " + message + "\n" +
                "\ttimestamp: " + timestamp + "\n" +
                (parent != null ? "\tparent: " + parent + "\n" : "") +
                "}";
    }
}
