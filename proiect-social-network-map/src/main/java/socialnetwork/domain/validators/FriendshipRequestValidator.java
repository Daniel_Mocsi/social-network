package socialnetwork.domain.validators;

import socialnetwork.domain.FriendshipRequest;

public class FriendshipRequestValidator implements Validator<FriendshipRequest> {
    @Override
    public void validate(FriendshipRequest entity) throws ValidationException {
        if (entity.getId().getLeft().equals(entity.getId().getRight()))
            throw new ValidationException("User cannot send a friendship request to themselves!");
    }
}
