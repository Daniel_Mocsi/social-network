package socialnetwork.domain.validators;

import socialnetwork.domain.User;

public class UserValidator implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {
        String errorMessage = "";
        if (entity.getFirstName() == null || entity.getFirstName().equals(""))
            errorMessage += "First name cannot be null!\n";
        if (entity.getLastName() == null || entity.getLastName().equals(""))
            errorMessage += "Last name cannot be null!\n";

        if (!errorMessage.equals(""))
            throw new ValidationException(errorMessage);

        if (!entity.getFirstName().matches("^[A-Z][a-z]+$"))
            errorMessage += "First name can only contain letters from the english alphabet and must start with a capital letter!\n";
        if (!entity.getLastName().matches("^[A-Z][a-z]+$"))
            errorMessage += "Last name can only contain letters from the english alphabet and must start with a capital letter!\n";

        if (!errorMessage.equals(""))
            throw new ValidationException(errorMessage);
    }

}
