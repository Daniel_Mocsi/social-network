package socialnetwork.domain.validators;

import socialnetwork.domain.Friendship;

import java.time.LocalDateTime;

public class FriendshipValidator implements Validator<Friendship> {
    @Override
    public void validate(Friendship entity) throws ValidationException {
        String errorMessage = "";
        if (entity.getId().getLeft().equals(entity.getId().getRight()))
            errorMessage += "One cannot be friends with themselves!\n";

        if (!errorMessage.equals(""))
            throw new ValidationException(errorMessage);
    }
}
