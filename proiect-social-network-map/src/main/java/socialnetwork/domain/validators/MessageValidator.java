package socialnetwork.domain.validators;

import socialnetwork.domain.Message;

public class MessageValidator implements Validator<Message> {
    @Override
    public void validate(Message entity) throws ValidationException {
        String str = "";
        if (entity.getMessage().contains(";"))
            str += "Messages cannot contain semicolons(;)!\n";
        if (entity.getTo().contains(entity.getFrom()))
            str += "Cannot send message to oneself!";

        if (!str.equals(""))
            throw new ValidationException(str);
    }
}
