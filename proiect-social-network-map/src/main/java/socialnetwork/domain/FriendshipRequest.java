package socialnetwork.domain;

import socialnetwork.utils.FriendshipRequestStatus;

import java.util.Objects;

public class FriendshipRequest extends Entity<Tuple<Long, Long>> {
    private FriendshipRequestStatus status;

    public FriendshipRequest(FriendshipRequestStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId().getLeft(), getId().getRight(), status);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof FriendshipRequest) {
            FriendshipRequest that = (FriendshipRequest) obj;
            return getId().getLeft().equals(that.getId().getLeft()) && getId().getRight().equals(that.getId().getRight()) && status.equals(that.status);
        }
        return false;
    }

    @Override
    public String toString() {
        return "FriendshipRequest { from: " + getId().getLeft() + ", to: " + getId().getRight() + ", status: " + status.toString() + " }";
    }

    public FriendshipRequestStatus getStatus() {
        return status;
    }

    public void setStatus(FriendshipRequestStatus status) {
        this.status = status;
    }
}
