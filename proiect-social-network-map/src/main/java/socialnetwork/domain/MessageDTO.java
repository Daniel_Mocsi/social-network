package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MessageDTO extends Entity<Long> {
    private User from;
    private List<User> to;
    private String message;
    private LocalDateTime timestamp;
    private Long parent;

    public MessageDTO(Long id, User from, List<User> to, String message, LocalDateTime timestamp, Long parent) {
        setId(id);
        this.from = from;
        this.to = to;
        this.message = message;
        this.timestamp = timestamp;
        this.parent = parent;
    }

    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    @Override
    public String toString() {
        String str = "id: " + getId() + ", " + timestamp.format(Constants.DATE_TIME_FORMATTER) + " " + from.getFirstName() + " " + from.getLastName() + ":\n";
        if (to.size() > 1)
            str += "to: " + to.stream()
                    .map(u -> u.getFirstName() + " " + u.getLastName())
                    .collect(Collectors.joining(", ")) + "\n";
        if (parent != null)
            str += "parent: " + parent + "\n";
        str += "\t" + message + "\n";
        return str;
    }
}
