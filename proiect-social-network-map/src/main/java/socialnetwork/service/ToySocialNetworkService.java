package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.file.FriendshipFileRepository;
import socialnetwork.repository.file.FriendshipRequestFileRepository;
import socialnetwork.repository.file.MessageFileRepository;
import socialnetwork.repository.file.UserFileRepository;
import socialnetwork.utils.FriendshipRequestStatus;
import socialnetwork.utils.Graph;

import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ToySocialNetworkService {
    private UserFileRepository userFileRepository;
    private FriendshipFileRepository friendshipFileRepository;
    private MessageFileRepository messageFileRepository;
    private FriendshipRequestFileRepository friendshipRequestFileRepository;

    public ToySocialNetworkService(UserFileRepository userFileRepository,
                                   FriendshipFileRepository friendshipFileRepository,
                                   MessageFileRepository messageFileRepository,
                                   FriendshipRequestFileRepository friendshipRequestFileRepository) {
        this.userFileRepository = userFileRepository;
        this.friendshipFileRepository = friendshipFileRepository;
        this.messageFileRepository = messageFileRepository;
        this.friendshipRequestFileRepository = friendshipRequestFileRepository;
        this.userFileRepository.addObserver(friendshipFileRepository);
        this.userFileRepository.addObserver(friendshipRequestFileRepository);
        updateFriends();
    }

    private void updateFriends() {
        for (Friendship f : getAllFriendships()) {
            User u1 = userFileRepository.findOne(f.getId().getLeft()).get();
            User u2 = userFileRepository.findOne(f.getId().getRight()).get();
            u1.getFriends().add(u2);
            u2.getFriends().add(u1);
        }
    }

    /**
     * gets all the users in the repository
     * @return an Iterable of Users
     */
    public Iterable<User> getAllUsers() {
        return userFileRepository.getAll();
    }

    /**
     * gets all friendships in the repository
     * @return an Iterable of Friendships
     */
    public Iterable<Friendship> getAllFriendships() {
        return friendshipFileRepository.getAll();
    }

    /**
     * Adds an user to the repository
     * @param firstName the first name of the user
     * @param lastName the last name of the user
     */
    public void addUser(String firstName, String lastName) {
        User u = new User(firstName, lastName);

        Random random = new Random();
        u.setId(random.nextLong() & Long.MAX_VALUE);

        Optional<User> user = userFileRepository.add(u);
        if (user.isPresent())
            addUser(firstName, lastName);
    }

    /**
     * Removes an user from the repository by name
     * @param firstName the first name of the user
     * @param lastName the last name of the user
     * @throws ServiceException if the user could not be found or if there are multiple users with the same name
     */
    public void removeUser(String firstName, String lastName) {
        Optional<User> res = userFileRepository.findUniqueByName(firstName, lastName);
        if (res.isPresent()) {
            removeUser(res.get().getId());
            return;
        }
        throw new ServiceException("User with the given name does not exist!");
    }

    /**
     * Removes an user from the repository by name
     * @param id the id of the user
     * @throws ServiceException if the user could not be found
     */
    public void removeUser(Long id) {
        Optional<User> user = userFileRepository.remove(id);
        if (user.isEmpty())
            throw new ServiceException("Could not find user with given ID!");
    }

    public void updateUser(Long id, String firstName, String lastName) {
        User u = new User(firstName, lastName);
        u.setId(id);
        Optional<User> user = userFileRepository.update(u);

        if (user.isPresent())
            throw new ServiceException("Could not find user with given ID!");
    }

    /**
     * Gets a tuple of 2 users given their names
     * @param firstName1 the first name of the first user
     * @param lastName1 the last name of the first user
     * @param firstName2 the first name of the second user
     * @param lastName2 the last name of the second user
     * @return a tuple containing the 2 users
     * @throws ServiceException if one of the users does not exist or there are multiple users with the same name
     */
    public Tuple<User, User> getUsers(String firstName1, String lastName1, String firstName2, String lastName2) {
        Optional<User> user1 = userFileRepository.findUniqueByName(firstName1, lastName1);
        Optional<User> user2 = userFileRepository.findUniqueByName(firstName2, lastName2);

        return getUsers(user1, user2);
    }

    /**
     * Gets a tuple of 2 users given their IDs
     * @param id1 the first name of the first user
     * @param id2 the last name of the first user
     * @return a tuple containing the 2 users
     * @throws ServiceException if one of the users does not exist
     */
    public Tuple<User, User> getUsers(Long id1, Long id2) {
        Optional<User> user1 = userFileRepository.findOne(id1);
        Optional<User> user2 = userFileRepository.findOne(id2);

        return getUsers(user1, user2);
    }

    private Tuple<User, User> getUsers(Optional<User> user1, Optional<User> user2) {
        if (user1.isPresent() && user2.isPresent())
            return new Tuple<>(user1.get(), user2.get());

        String errorMessage = "";
        if (user1.isEmpty())
            errorMessage += "First user does not exist!\n";
        if (user2.isEmpty())
            errorMessage += "Second user does not exist!\n";
        throw new ServiceException(errorMessage);
    }

    /**
     * Adds a new friendship given the names of the users
     * @param firstName1 the first name of the first user
     * @param lastName1 the last name of the first user
     * @param firstName2 the first name of the second user
     * @param lastName2 the last name of the second user
     */
    public void addFriendship(String firstName1, String lastName1, String firstName2, String lastName2) {
        Tuple<User, User> userTuple = getUsers(firstName1, lastName1, firstName2, lastName2);
        addFriendship(userTuple.getLeft(), userTuple.getRight(), LocalDateTime.now());
    }

    /**
     * Adds a new friendship given the IDs of the users
     * @param id1 the first name of the first user
     * @param id2 the last name of the first user
     */
    public void addFriendship(Long id1, Long id2) {
        Tuple<User, User> userTuple = getUsers(id1, id2);
        addFriendship(userTuple.getLeft(), userTuple.getRight(), LocalDateTime.now());
    }

    /**
     * Adds a new friendship given the users
     * @param user1 the first user
     * @param user2 the second user
     * @param date the date when the friendship started which has to be in the past
     */
    private void addFriendship(User user1, User user2, LocalDateTime date) {
        user1.getFriends().add(user2);
        user2.getFriends().add(user1);

        Friendship f = new Friendship(date);
        f.setId(new Tuple<>(user1.getId(), user2.getId()));
        Optional<Friendship> fs = friendshipFileRepository.add(f);
        fs.ifPresent(x -> { throw new ServiceException("Friendship already exists!"); });
    }

    /**
     * Removes friendship given the names of the users
     * @param firstName1 the first name of the first user
     * @param lastName1 the last name of the first user
     * @param firstName2 the first name of the second user
     * @param lastName2 the last name of the second user
     */
    public void removeFriendship(String firstName1, String lastName1, String firstName2, String lastName2) {
        Tuple<User, User> userTuple = getUsers(firstName1, lastName1, firstName2, lastName2);
        removeFrienship(userTuple.getLeft(), userTuple.getRight());
    }

    /**
     * Removes a friendship given the IDs of the users
     * @param id1 the first name of the first user
     * @param id2 the last name of the first user
     */
    public void removeFriendship(Long id1, Long id2) {
        Tuple<User, User> userTuple = getUsers(id1, id2);
        removeFrienship(userTuple.getLeft(), userTuple.getRight());
    }

    /**
     * Removes a friendship given the 2 users
     * @param user1 the first user
     * @param user2 the second user
     */
    private void removeFrienship(User user1, User user2) {
        Optional<Friendship> f = friendshipFileRepository.remove(new Tuple<>(user1.getId(), user2.getId()));
        if (f.isPresent()) {
            user1.getFriends().remove(user2);
            user2.getFriends().remove(user1);
            return;
        }
        throw new ServiceException("Users are not friends!");
    }

    /**
     * counts the communities according to the friendships
     */
    public int countCommunities() {
        ArrayList<User> nodes = new ArrayList<>();
        userFileRepository.getAll().forEach(nodes::add);
        HashMap<Long, ArrayList<Long>> adjList = new HashMap<>();
        for (User node : nodes) {
            ArrayList<Long> neighbours = adjList.get(node.getId());
            if (neighbours == null)
                neighbours = new ArrayList<>();

            for (User friend : node.getFriends()) {
                neighbours.add(friend.getId());
                ArrayList<Long> nb = adjList.get(friend.getId());
                if (nb == null) {
                    nb = new ArrayList<>();
                    nb.add(node.getId());
                    adjList.put(friend.getId(), nb);
                }
                else
                    adjList.get(friend.getId()).add(node.getId());
            }
            adjList.put(node.getId(), neighbours);
        }

        Graph g = new Graph(adjList.size());
        g.setAdjList(adjList);
        return g.countConnectedComponents();
    }

    /**
     * Gets the friend list for a given user
     * @param id the user's id
     * @return an Iterable of FriendDTOs which can be printed to a console
     */
    public Iterable<FriendDTO> getFriends(Long id) {
        Optional<User> user = userFileRepository.findOne(id);
        if (user.isPresent())
            return getFriends(user.get());

        throw new ServiceException("User does not exist!");
    }

    /**
     * Gets the friend list for a given user
     * @param firstName the first name of the user
     * @param lastName the second name of the user
     * @return an Iterable of FriendDTOs which can be printed to a console
     */
    public Iterable<FriendDTO> getFriends(String firstName, String lastName) {
        Optional<User> user = userFileRepository.findUniqueByName(firstName, lastName);
        if (user.isPresent())
            return getFriends(user.get());

        throw new ServiceException("User does not exist!");
    }

    /**
     * Gets the friend list for a given user
     * @param user the user
     * @return an Iterable of FriendDTOs which can be printed to a console
     */
    private Iterable<FriendDTO> getFriends(User user) {
        Collection<Friendship> friendships = ((Collection<Friendship>) friendshipFileRepository.getAll());
        return friendships.stream()
                .filter(f -> f.getId().getLeft().equals(user.getId()) || f.getId().getRight().equals(user.getId()))
                .map(f -> {
                    Long id = null;
                    if (f.getId().getLeft().equals(user.getId()))
                        id = f.getId().getRight();
                    else if (f.getId().getRight().equals(user.getId()))
                        id = f.getId().getLeft();
                    Optional<User> usr = userFileRepository.findOne(id);
                    if (usr.isPresent()) {
                        User u = usr.get();
                        return new FriendDTO(u.getFirstName(), u.getLastName(), f.getDate());
                    }
                    throw new ServiceException("User repository data does not match friendship repository data!");
                })
                .collect(Collectors.toList());
    }

    /**
     * gets the friends for a given user with whom he made friends in a given month
     * @param id the id of the user
     * @param month the month
     * @return an Iterable of FriendDTOs which can be printed to a console
     */
    public Iterable<FriendDTO> getFriendsMonth(Long id, int month) {
        List<FriendDTO> friends = (List<FriendDTO>) getFriends(id);
        return friends.stream()
                .filter(f -> f.getDate().getMonth().getValue() == month)
                .collect(Collectors.toList());
    }

    /**
     * gets the friends for a given user with whom he made friends in a given month
     * @param firstName the first name of the user
     * @param lastName the last name of the user
     * @param month
     * @return
     */
    public Iterable<FriendDTO> getFriendsMonth(String firstName, String lastName, int month) {
        List<FriendDTO> friends = (List<FriendDTO>) getFriends(firstName, lastName);
        return friends.stream()
                .filter(f -> f.getDate().getMonth().getValue() == month)
                .collect(Collectors.toList());
    }

    /**
     * sends a message to a user
     * @param from the user that is sending the message
     * @param to the user(s) the messages are for
     * @param message the message
     * @param parent the message the sender is replying to (can be null)
     */
    public void sendMessage(Long from, List<Long> to, String message, Long parent) {
        Message m;
        String ex = "";
        Optional<User> f = userFileRepository.findOne(from);
        if (f.isEmpty())
            ex += "Sender does not exist in the user repository!\n";
        for (Long l : to)
            if (userFileRepository.findOne(l).isEmpty())
                 ex += "Recipient with ID: " + l + " not found!\n";
        if (parent != null) {
            Optional<Message> p = messageFileRepository.findOne(parent);
            if (p.isEmpty())
                ex += "Parent message not found!\n";
            m = new Message(from, to, message, LocalDateTime.now(), parent);
        }
        else m = new Message(from, to, message, LocalDateTime.now());

        if (!ex.equals(""))
            throw new ServiceException(ex);

        Random random = new Random();
        m.setId(random.nextLong() & Long.MAX_VALUE);

        Optional<Message> msg = messageFileRepository.add(m);
        if (msg.isPresent())
            sendMessage(from, to, message, parent);
    }

    /**
     * gets the messages between 2 users in chronological order
     * @param id1 the id of the first user
     * @param id2 the id of the second user
     * @return an Iterable of MessageDTOs which can be printed to a console
     */
    public Iterable<MessageDTO> getConversation(Long id1, Long id2) {
        return ((Collection<Message>) messageFileRepository.getAll()).stream()
                .filter(m -> m.getFrom().equals(id1) && m.getTo().contains(id2)
                        || m.getFrom().equals(id2) && m.getTo().contains(id1))
                .map(m -> new MessageDTO(
                        m.getId(),
                        userFileRepository.findOne(m.getFrom()).get(),
                        m.getTo().stream()
                            .map(l -> userFileRepository.findOne(l).get())
                            .collect(Collectors.toList()),
                        m.getMessage(),
                        m.getTimestamp(),
                        m.getParent()))
                .sorted(Comparator.comparing(MessageDTO::getTimestamp))
                .collect(Collectors.toList());
    }

    /**
     * gets the friendship requests associated with a given user
     * @param id the id of the user
     * @return an Iterable containing the friendship requests associated with the user
     */
    public Iterable<FriendshipRequest> getFriendshipRequests(Long id) {
        return ((Collection<FriendshipRequest>) friendshipRequestFileRepository.getAll()).stream()
                .filter(fr -> fr.getId().getRight().equals(id) || fr.getId().getLeft().equals(id))
                .collect(Collectors.toList());
    }

    /**
     * sends a friendship request to an user
     * @param from the sender
     * @param to the recipient
     */
    public void sendFriendshipRequest(Long from, Long to) {
        String ex = "";
        Optional<User> f = userFileRepository.findOne(from);
        if (f.isEmpty())
            ex += "Sender does not exist in the user repository!\n";
        Optional<User> t = userFileRepository.findOne(to);
        if (t.isEmpty())
            ex += "Recipient does not exist in the user repository!\n";
        if (!ex.equals(""))
            throw new ServiceException(ex);

        Optional<FriendshipRequest> rev = friendshipRequestFileRepository.findOne(new Tuple<>(to, from));
        if (rev.isPresent())
            throw new ServiceException("The recipient has already sent a friend request to the sender!\n");
        Optional<Friendship> exists = friendshipFileRepository.findOne(new Tuple<>(to, from));
        if (exists.isEmpty())
            exists = friendshipFileRepository.findOne(new Tuple<>(from, to));
        if (exists.isPresent())
            throw new ServiceException("Users are already friends!");

        FriendshipRequest fr = new FriendshipRequest(FriendshipRequestStatus.PENDING);
        fr.setId(new Tuple<>(from, to));
        friendshipRequestFileRepository.add(fr);
    }

    /**
     * updates the status of a given friendship request
     * @param from the user who sent the friendship request
     * @param to the user who received the friendship request
     * @param status the response to the friendship request (ACCEPTED|REJECTED)
     */
    public void updateFriendshipRequest(Long from, Long to, FriendshipRequestStatus status) {
        Optional<FriendshipRequest> fr = friendshipRequestFileRepository.findOne(new Tuple<>(from, to));
        if (fr.isEmpty())
            throw new ServiceException("Could not find friendship request!");
        if (!fr.get().getStatus().equals(FriendshipRequestStatus.PENDING))
            throw new ServiceException("Request has already been responded to!");
        fr.get().setStatus(status);
        friendshipRequestFileRepository.update(fr.get());
        if (status.equals(FriendshipRequestStatus.ACCEPTED))
            addFriendship(from, to);
    }
}
