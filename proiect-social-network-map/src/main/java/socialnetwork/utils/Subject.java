package socialnetwork.utils;

import socialnetwork.domain.User;

public interface Subject {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyAll(User entity);
}
