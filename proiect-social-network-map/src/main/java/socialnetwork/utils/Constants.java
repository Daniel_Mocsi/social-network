package socialnetwork.utils;

import java.time.format.DateTimeFormatter;

public final class Constants {
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
    public static final DateTimeFormatter DATE_TIME_FORMATTER_PRECISE = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss:AA");
}
