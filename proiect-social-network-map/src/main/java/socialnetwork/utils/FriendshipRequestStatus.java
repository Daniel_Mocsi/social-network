package socialnetwork.utils;

public enum FriendshipRequestStatus {
    PENDING,
    ACCEPTED,
    REJECTED
}
