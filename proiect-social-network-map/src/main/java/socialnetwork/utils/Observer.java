package socialnetwork.utils;

import socialnetwork.domain.User;

public interface Observer {
    void onUserRemoved(User entity);
}
