package socialnetwork.repository.memory;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.RepositoryException;

import javax.swing.text.html.Option;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID, E> {

    private Validator<E> validator;
    protected Map<ID, E> entities;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities = new HashMap<ID, E>();
    }

    /**
     *
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the matching entity or null if not found
     * @throws RepositoryException if id is null
     */
    @Override
    public Optional<E> findOne(ID id) {
        if (id == null)
            throw new RepositoryException("ID cannot be null!");
        return Optional.ofNullable(entities.get(id));
    }

    /**
     * @return An iterable of the entities in the repository
     */
    @Override
    public Iterable<E> getAll() {
        return entities.values();
    }

    /**
     * Adds an entity to the repository
     * @param entity entity must be not null
     * @return null if the entity was added successfully or the entity if it already exists
     */
    @Override
    public Optional<E> add(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity cannot be null!");

        validator.validate(entity);

        if (entities.get(entity.getId()) != null)
            return Optional.of(entity);

        entities.put(entity.getId(), entity);
        return Optional.empty();
    }

    /**
     * Removes an entity from the repository
     * @param id id must be not null, the ID of the entity to be removed
     * @return the entity if it was successfully remove, otherwise null
     * @throws RepositoryException if the id is null
     */
    @Override
    public Optional<E> remove(ID id) {
        if (id == null)
            throw new RepositoryException("ID cannot be null!");

        return Optional.ofNullable(entities.remove(id));
    }

    /**
     * Updates the attributes of an entity
     * @param entity entity must not be null, must contain the ID of the entity to be updated
     * @return an entity containing the old attributes upon success, null otherwise
     */
    @Override
    public Optional<E> update(E entity) {
        if (entity == null)
            throw new RepositoryException("entity must be not null!");

        validator.validate(entity);

        entities.put(entity.getId(), entity);

        if (entities.get(entity.getId()) != null) {
            entities.put(entity.getId(), entity);
            return Optional.empty();
        }

        return Optional.of(entity);
    }
}
