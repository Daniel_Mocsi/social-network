package socialnetwork.repository.file;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Iterator;

public class MessageFileRepository extends AbstractFileRepository<Long, Message> {
    public MessageFileRepository(String fileName, Validator<Message> validator) {
        super(fileName, validator);
    }

    @Override
    public Message extractEntity(List<String> attributes) {
        Iterator<String> attrIterator = attributes.iterator();
        Long id = Long.parseLong(attrIterator.next());
        Long from = Long.parseLong(attrIterator.next());
        String message = attrIterator.next();
        LocalDateTime timestamp = LocalDateTime.parse(attrIterator.next(), Constants.DATE_TIME_FORMATTER_PRECISE);
        Long parent = Long.parseLong(attrIterator.next());
        if (parent == 0)
            parent = null;
        List<Long> to = new ArrayList<>();
        while (attrIterator.hasNext())
            to.add(Long.parseLong(attrIterator.next()));

        Message msg = new Message(from, to, message, timestamp, parent);
        msg.setId(id);
        return msg;
    }

    @Override
    protected String createEntityAsString(Message entity) {
        String str = entity.getId() + ";" + entity.getFrom() + ";" + entity.getMessage() + ";" + entity.getTimestamp().format(Constants.DATE_TIME_FORMATTER_PRECISE) + ";" + (entity.getParent() != null ? entity.getParent() : 0) + ";";
        str += entity.getTo().stream()
                .map(Object::toString)
                .collect(Collectors.joining(";"));
        return str;
    }
}
