package socialnetwork.repository.file;

import socialnetwork.domain.FriendshipRequest;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.utils.FriendshipRequestStatus;
import socialnetwork.utils.Observer;

import java.util.List;

public class FriendshipRequestFileRepository extends AbstractFileRepository<Tuple<Long, Long>, FriendshipRequest> implements Observer {
    public FriendshipRequestFileRepository(String fileName, Validator<FriendshipRequest> validator) {
        super(fileName, validator);
    }

    /**
     * Creates a friendship requests given its attributes
     * @param attributes a list containing all the attributes needed for the entity as strings
     * @return the generated friendship request
     */
    @Override
    public FriendshipRequest extractEntity(List<String> attributes) {
        FriendshipRequest fr = new FriendshipRequest(FriendshipRequestStatus.valueOf(attributes.get(2)));
        fr.setId(new Tuple<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1))));
        return fr;
    }

    /**
     * Serializes a friendship request for file storage
     * @param entity the entity for which we create the string
     * @return the string for the given friendship request
     */
    @Override
    protected String createEntityAsString(FriendshipRequest entity) {
        return entity.getId().getLeft() + ";" + entity.getId().getRight() + ";" + entity.getStatus().toString();
    }

    /**
     * removes the friendship requests associated with a removed user
     * @param entity the removed entity
     */
    @Override
    public void onUserRemoved(User entity) {
        entities.keySet().removeIf(k -> k.getLeft().equals(entity.getId()) || k.getRight().equals(entity.getId()));
        updateFile();
    }
}
