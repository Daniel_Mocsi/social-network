package socialnetwork.repository.file;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepositoryException;
import socialnetwork.utils.Observer;
import socialnetwork.utils.Subject;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserFileRepository extends AbstractFileRepository<Long, User> implements Subject {
    private List<Observer> observers = new ArrayList<>();

    public UserFileRepository(String fileName, Validator<User> validator) {
        super(fileName, validator);
    }

    public Optional<User> findUniqueByName(String firstName, String lastName) {
        List<User> users = entities.values()
                .stream()
                .filter(u -> u.getFirstName().equals(firstName) && u.getLastName().equals(lastName))
                .collect(Collectors.toList());
        if (users.size() > 1)
            throw new RepositoryException("Found multiple users with the same name!");
        return Optional.ofNullable(users.size() == 1 ? users.get(0) : null);
    }

    /**
     * Removes the user from the repository and their associated friendships
     * @param id id must be not null, the ID of the entity to be removed
     * @return
     */
    @Override
    public Optional<User> remove(Long id) {
        Optional<User> user = super.remove(id);
        user.ifPresent(this::notifyAll);
        return user;
    }

    @Override
    public User extractEntity(List<String> attributes) {
        User user = new User(attributes.get(1), attributes.get(2));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
    }

    @Override
    protected String createEntityAsString(User entity) {
        return entity.getId() + ";" + entity.getFirstName() + ";" + entity.getLastName();
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    /**
     * Notifies the friendship repository about the removal of an user
     * @param entity the entity that has been removed
     */
    @Override
    public void notifyAll(User entity) {
        for (Observer o : observers) {
            o.onUserRemoved(entity);
        }
    }
}
