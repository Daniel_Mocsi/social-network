package socialnetwork.repository.file;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.utils.Constants;
import socialnetwork.utils.Observer;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class FriendshipFileRepository extends AbstractFileRepository<Tuple<Long, Long>, Friendship> implements Observer {
    public FriendshipFileRepository(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
    }

    @Override
    public Friendship extractEntity(List<String> attributes) {
        if (attributes.size() == 3) {
            Friendship friendship = new Friendship(LocalDateTime.parse(attributes.get(2), Constants.DATE_TIME_FORMATTER));
            Tuple<Long, Long> friendshipID = new Tuple<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1)));
            friendship.setId(friendshipID);
            return friendship;
        }
        return null;
    }

    @Override
    protected String createEntityAsString(Friendship entity) {
        return entity.getId().getLeft() + ";" + entity.getId().getRight() + ";" + entity.getDate().format(Constants.DATE_TIME_FORMATTER);
    }

    /**
     * Removes the friendships containing removed users
     * @param entity the entity that has been removed
     */
    @Override
    public void onUserRemoved(User entity) {
        entities.keySet().removeIf(k -> k.getLeft().equals(entity.getId()) || k.getRight().equals(entity.getId()));
        updateFile();
    }
}
