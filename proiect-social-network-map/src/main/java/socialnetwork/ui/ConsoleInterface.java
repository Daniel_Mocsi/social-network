package socialnetwork.ui;

import socialnetwork.domain.FriendDTO;
import socialnetwork.domain.FriendshipRequest;
import socialnetwork.domain.MessageDTO;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.RepositoryException;
import socialnetwork.service.ServiceException;
import socialnetwork.service.ToySocialNetworkService;
import socialnetwork.utils.Constants;
import socialnetwork.utils.FriendshipRequestStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class ConsoleInterface {
    private ToySocialNetworkService service;
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public void setService(ToySocialNetworkService service) {
        this.service = service;
    }

    public void run() {
        int option = 0;
        boolean running = true;

        System.out.println("ToySocialNetwork - Lab 3");
        System.out.println();
        System.out.println("1. Show all users;");
        System.out.println("2. Show all frienships;");
        System.out.println("3. Add a new user;");
        System.out.println("4. Remove a user;");
        System.out.println("5. Add a friendship;");
        System.out.println("6. Remove a friendship;");
        System.out.println("7. Get number of communities;");
        System.out.println("8. Show a user's friends;");
        System.out.println("9. Show a user's friends by month;");
        System.out.println("10. Send a new message;");
        System.out.println("11. View conversation between 2 users;");
        System.out.println("12. View friendship requests;");
        System.out.println("13. Send new friendship request");
        System.out.println("14. Respond to friendship request;");
        System.out.println("0. Exit;");
        while (running) {
            System.out.print("Choose an option: ");
            try {
                option = Integer.parseInt(in.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println();
            try {
                switch (option) {
                    case 1:
                        showUsers();
                        break;
                    case 2:
                        showFriendships();
                        break;
                    case 3:
                        addUser();
                        break;
                    case 4:
                        removeUser();
                        break;
                    case 5:
                        addFriendship();
                        break;
                    case 6:
                        removeFrienship();
                        break;
                    case 7:
                        countCommunities();
                        break;
                    case 8:
                        showFriends();
                        break;
                    case 9:
                        showFriendsMonth();
                        break;
                    case 10:
                        sendMessage();
                        break;
                    case 11:
                        showConversation();
                        break;
                    case 12:
                        showFriendshipRequests();
                        break;
                    case 13:
                        sendFriendshipRequest();
                        break;
                    case 14:
                        updateFriendshipRequest();
                        break;
                    case 0:
                        running = false;
                        break;
                    default:
                        System.out.println("Invalid option!");
                }
            } catch (IOException | DateTimeParseException | IllegalArgumentException | ValidationException | RepositoryException | ServiceException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void showUsers() {
        service.getAllUsers().forEach(System.out::println);
        System.out.println();
    }

    private void showFriendships() {
        service.getAllFriendships().forEach(System.out::println);
        System.out.println();
    }

    private void addUser() throws IOException {
        System.out.print("First name: ");
        String firstName = in.readLine();
        System.out.print("Last name: ");
        String lastName = in.readLine();
        service.addUser(firstName, lastName);
        System.out.println("User added successfully!");
    }

    private void removeUser() throws IOException {
        System.out.println("1. Remove by name;");
        System.out.println("2. Remove by ID;");
        System.out.println();
        System.out.print("Choose an option: ");
        int option = Integer.parseInt(in.readLine());
        System.out.println();
        switch (option) {
            case 1:
                System.out.print("First name: ");
                String firstName = in.readLine();
                System.out.print("Last name: ");
                String lastName = in.readLine();
                service.removeUser(firstName, lastName);
                break;
            case 2:
                System.out.print("ID: ");
                Long id = Long.parseLong(in.readLine());
                service.removeUser(id);
                break;
            default:
                System.out.println("Invalid option!");
        }
        System.out.println("User removed successfully!");
    }

    private void addFriendship() throws IOException {
        System.out.println("1. Add by names;");
        System.out.println("2. Add by IDs;");
        System.out.println();
        System.out.print("Choose an option: ");
        int option = Integer.parseInt(in.readLine());
        System.out.println();
        LocalDateTime date;
        switch (option) {
            case 1:
                System.out.println("First user: ");
                System.out.print("\tFirst name: ");
                String firstName1 = in.readLine();
                System.out.print("\tLast name: ");
                String lastName1 = in.readLine();
                System.out.println("Second user: ");
                System.out.print("\tFirst name: ");
                String firstName2 = in.readLine();
                System.out.print("\tLast name: ");
                String lastName2 = in.readLine();
                service.addFriendship(firstName1, lastName1,  firstName2, lastName2);
                break;
            case 2:
                System.out.println("First user: ");
                System.out.print("\tID: ");
                Long id1 = Long.parseLong(in.readLine());
                System.out.println("Second user: ");
                System.out.print("\tID: ");
                Long id2 = Long.parseLong(in.readLine());
                service.addFriendship(id1, id2);
                break;
            default:
                System.out.println("Invalid option!");
        }
        System.out.println("Friendship added successfully!");
    }

    private void removeFrienship() throws IOException {

        System.out.println("1. Remove by names;");
        System.out.println("2. Remove by IDs;");
        System.out.println();
        System.out.print("Choose an option: ");
        int option = Integer.parseInt(in.readLine());
        System.out.println();
        LocalDateTime date;
        switch (option) {
            case 1:
                System.out.println("First user: ");
                System.out.print("\tFirst name: ");
                String firstName1 = in.readLine();
                System.out.print("\tLast name: ");
                String lastName1 = in.readLine();
                System.out.println("Second user: ");
                System.out.print("\tFirst name: ");
                String firstName2 = in.readLine();
                System.out.print("\tLast name: ");
                String lastName2 = in.readLine();
                service.removeFriendship(firstName1, lastName1,  firstName2, lastName2);
                break;
            case 2:
                System.out.println("First user: ");
                System.out.print("\tID: ");
                Long id1 = Long.parseLong(in.readLine());
                System.out.println("Second user: ");
                System.out.print("\tID: ");
                Long id2 = Long.parseLong(in.readLine());
                service.removeFriendship(id1, id2);
                break;
            default:
                System.out.println("Invalid option!");
        }
        System.out.println("Friendship removed successfully!");
    }

    private void countCommunities() {
        System.out.println("The number of communities is " + service.countCommunities());
    }
    
    private void showFriends() throws IOException {
        System.out.println("1. Find user by name;");
        System.out.println("2. Find user by ID;");
        System.out.println();
        System.out.print("Choose an option: ");
        int option = Integer.parseInt(in.readLine());
        System.out.println();

        Iterable<FriendDTO> res = null;
        switch (option) {
            case 1:
                System.out.print("First name: ");
                String firstName = in.readLine();
                System.out.print("Last name: ");
                String lastName = in.readLine();
                res = service.getFriends(firstName, lastName);
                break;
            case 2:
                System.out.print("ID: ");
                Long id = Long.parseLong(in.readLine());
                res = service.getFriends(id);
                break;
            default:
                System.out.println("Invalid option!");
        }
        if (res != null)
            res.forEach(System.out::println);
        else
            System.out.println("No friends found!");
        System.out.println();
    }

    private void showFriendsMonth() throws IOException {
        System.out.println("1. Find user by name;");
        System.out.println("2. Find user by ID;");
        System.out.println();
        System.out.print("Choose an option: ");
        int option = Integer.parseInt(in.readLine());
        System.out.println();

        Iterable<FriendDTO> res = null;
        int month;
        switch (option) {
            case 1:
                System.out.print("First name: ");
                String firstName = in.readLine();
                System.out.print("Last name: ");
                String lastName = in.readLine();
                System.out.print("Month (number): ");
                month = Integer.parseInt(in.readLine());
                res = service.getFriendsMonth(firstName, lastName, month);
                break;
            case 2:
                System.out.print("ID: ");
                Long id = Long.parseLong(in.readLine());
                System.out.print("Month (number): ");
                month = Integer.parseInt(in.readLine());
                res = service.getFriendsMonth(id, month);
                break;
            default:
                System.out.println("Invalid option!");
        }
        if (res != null)
            res.forEach(System.out::println);
        else
            System.out.println("No friends found!");
        System.out.println();
    }

    private void sendMessage() throws IOException {
        System.out.print("Message from (ID): ");
        Long from = Long.parseLong(in.readLine());
        List<Long> to = new ArrayList<>();
        long id;
        System.out.print("Message to (IDs, stops at 0): ");
        while ((id = Long.parseLong(in.readLine())) != 0) {
            to.add(id);
            System.out.print("Message to (IDs, stops at 0): ");
        }
        System.out.print("Message: ");
        String message = in.readLine();
        System.out.print("Parent (0 for none): ");
        Long parent = Long.parseLong(in.readLine());
        if (parent == 0)
            parent = null;
        service.sendMessage(from, to, message, parent);
        System.out.println("Message sent successfully!");
    }

    private void showConversation() throws IOException {
        System.out.print("First user ID: ");
        Long id1 = Long.parseLong(in.readLine());
        System.out.print("Second user ID: ");
        Long id2 = Long.parseLong(in.readLine());
        service.getConversation(id1, id2).forEach(System.out::println);
    }

    private void showFriendshipRequests() throws IOException {
        System.out.print("User ID to show requests for: ");
        Long id = Long.parseLong(in.readLine());
        service.getFriendshipRequests(id).forEach(System.out::println);
    }

    private void sendFriendshipRequest() throws IOException {
        System.out.print("Friendship request from: ");
        Long from = Long.parseLong(in.readLine());
        System.out.print("Friendship request to: ");
        Long to = Long.parseLong(in.readLine());
        service.sendFriendshipRequest(from, to);
        System.out.println("Friendship request sent!");
    }

    private void updateFriendshipRequest() throws IOException {
        System.out.print("Respond to friendship request from: ");
        Long from = Long.parseLong(in.readLine());
        System.out.print("to: ");
        Long to = Long.parseLong(in.readLine());
        System.out.println("Response (ACCEPTED|REJECTED): ");
        FriendshipRequestStatus status = FriendshipRequestStatus.valueOf(in.readLine());
        service.updateFriendshipRequest(from, to, status);
        System.out.println("Success!");
    }
}
