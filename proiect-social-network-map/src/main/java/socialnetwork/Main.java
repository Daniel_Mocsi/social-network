package socialnetwork;

import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.FriendshipRequestValidator;
import socialnetwork.domain.validators.FriendshipValidator;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.domain.validators.UserValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.FriendshipFileRepository;
import socialnetwork.repository.file.FriendshipRequestFileRepository;
import socialnetwork.repository.file.MessageFileRepository;
import socialnetwork.repository.file.UserFileRepository;
import socialnetwork.service.ToySocialNetworkService;
import socialnetwork.ui.ConsoleInterface;

public class Main {
    public static void main(String[] args) {
        String usersFileName = ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
        String friendshipsFileName = ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.frienships");
        String messagesFileName = ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.messages");
        String friendshipRequestsFileName = ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.friendshipRequests");

        UserFileRepository userRepository = new UserFileRepository(usersFileName, new UserValidator());
        FriendshipFileRepository friendshipRepository = new FriendshipFileRepository(friendshipsFileName, new FriendshipValidator());
        MessageFileRepository messageRepository = new MessageFileRepository(messagesFileName, new MessageValidator());
        FriendshipRequestFileRepository friendshipRequestFileRepository = new FriendshipRequestFileRepository(friendshipRequestsFileName, new FriendshipRequestValidator());

        ToySocialNetworkService service = new ToySocialNetworkService(userRepository, friendshipRepository, messageRepository, friendshipRequestFileRepository);
        ConsoleInterface consoleInterface = new ConsoleInterface();
        consoleInterface.setService(service);
        consoleInterface.run();
    }
}


